Open edX Devstack for local development (version juniper.master) |Editing Status|
================================

Get up and running quickly with Open edX services.

A Devstack installation includes the following Open edX components:

* The Learning Management System (LMS)
* Open edX Studio
* Discussion Forums
* Open Response Assessments (ORA)
* E-Commerce
* Credentials
* Notes
* Course Discovery
* XQueue
* Open edX Search
* A demonstration Open edX course

It also includes the following extra components:

* XQueue
* The components needed to run the Open edX Analytics Pipeline. This is the primary extract, transform, and load (ETL) tool that extracts and analyzes data from the other Open edX services.
* The Program Console micro-frontend
* edX Registrar service.

Prerequisites
-------------

You will need to have the following installed:

- make
- python 3
- docker

This project requires **Docker 17.06+ CE**.  We recommend Docker Stable, but Docker Edge should work as well.

**NOTE:** Switching between Docker Stable and Docker Edge will remove all images and settings. Don't forget to restore your memory setting and be prepared to provision.

`Docker for Windows`_ may work but has not been tested and is *not* supported.

If you are using Linux, use the ``overlay2`` storage driver, kernel version 4.0+ and *not* ``overlay``. To check which storage driver your ``docker-daemon`` uses, run the following command.

    docker info | grep -i 'storage driver'

Using the Latest Images
-----------------------
    make down
    make dev.pull
    make dev.up

This will stop any running devstack containers, pull the latest images, and then start all of the devstack containers.

Getting Started normal (Install)
---------------

All of the services can be run by following the steps below. For analyticstack, follow `Getting Started on Analytics`_.

1. Going to folder that include OpenEdx resources (Example: /root/user/edx):

2. Clone custom devstack repo:


    git clone -b open-release/juniper.master git@gitlab.com:TuanLinhChi/devstack.git

3. Going to folder devstack:


    cd devstack

4. Set OPENEDX_RELEASE param global:


    export OPENEDX_RELEASE=juniper.master

5. The Docker Compose file mounts a host volume for each service's executing code. The host directory defaults to be a sibling of this directory. Run following command to clone basic repos.


    make dev.clone https

6. Pull any changes made to the various images on which the devstack depends.


    make dev.pull

if you want to install Analytics, going to `Getting Started on Analytics`_ section

7. Run the provision command.

   **NOTE:** When running the provision command, databases for ecommerce and edxapp will be dropped and recreated.

   The username and password for the superusers are both ``edx``. You can access the services directly via Django admin at the ``/admin/`` path, or login via single sign-on at ``/login/``.


    make dev.provision

   This is expected to take a while, produce a lot of output from a bunch of steps, and finally end with ``Provisioning complete!``

7. Start the services. This command will mount the repositories

   **NOTE:** it may take up to 60 seconds for the LMS to start, even after the ``make dev.up`` command outputs ``done``.


    make dev.up

8. Done! Setup Successfully

Getting Started on Analytics
----------------------------

Analyticstack can be run by following the steps below.

**NOTE:** Since a Docker-based devstack runs many containers, you should configure Docker with a sufficient amount of resources. We find that `configuring Docker for Mac`_ with a minimum of 2 CPUs and 6GB of memory works well for **analyticstack**. If you intend on running other docker services besides analyticstack ( e.g. lms, studio etc ) consider setting higher memory.

1. Follow steps `1` to (end) `6` from `Getting Started`_ section.

2. Running the following commands:


    make pull.analytics_pipeline

3. Run the provision command to configure the analyticstack.


    make dev.provision.analytics_pipeline https

4. Start the analytics service. This command will mount the repositories

   **NOTE:** it may take up to 60 seconds for Hadoop services to start.


    make dev.up.analytics_pipeline

5. (No Need) To access the analytics pipeline shell, run the following command. All analytics pipeline job/workflows should be executed after accessing the shell.


    make analytics-pipeline-shell

6. (No Need) For running acceptance tests on docker analyticstack, follow the instructions in the `Running analytics acceptance tests in docker`_ guide.

7. (No Need) For troubleshooting docker analyticstack, follow the instructions in the `Troubleshooting docker analyticstack`_ guide.

8. Done! Setup Successfully

Usernames and Passwords
-----------------------

The provisioning script creates a Django superuser for every service.

    Email: edx@example.com
    Username: edx
    Password: edx

The LMS also includes demo accounts. The passwords for each of these accounts is ``edx``.
- ``staff@example.com`` An LMS and Studio user with course creation and editing permissions. This user is a course team member with the Admin role, which gives rights to work with the demonstration course in Studio, the LMS, and Insights.
- ``verified@example.com`` A student account that you can use to access the LMS for testing verified certificates.
- ``audit@example.com`` A student account that you can use to access the LMS for testing course auditing.
- ``honor@example.com`` A student account that you can use to access the LMS for testing honor code certificates.

Service List
------------

Each service is accessible at ``localhost`` on a specific port. The table below provides links to the homepage, API root, or API docs of each service.

The services marked as ``Default`` are provisioned/pulled/run whenever you run
``make dev.provision`` / ``make dev.pull`` / ``make dev.up``, respectively.

The extra services are provisioned/pulled/run when specifically requested (e.g.,
``make dev.provision.xqueue`` / ``make dev.pull.xqueue`` / ``make dev.up.xqueue``).

- `lms` - http://localhost:18000/ - Python/Django - Default
- `studio` - http://localhost:18010/ - Python/Django - Default
- `forum` - http://localhost:44567/api/v1/ - Ruby/Sinatra - Default
- `discovery` - http://localhost:18381/api-docs/ - Python/Django - Default
- `ecommerce` - http://localhost:18130/dashboard/ - Python/Django - Default
- `credentials` - http://localhost:18150/api/v2/ - Python/Django - Default
- `edx_notes_api` - http://localhost:18120/api/v1/ - Python/Django - Default
- `frontend-app-publisher` - http://localhost:18400/ - MFE (React.js) - Default
- `gradebook` - http://localhost:1994/ - MFE (React.js) - Default
- `registrar` - http://localhost:18734/api-docs/ - Python/Django - Extra
- `program-console` - http://localhost:1976/ - MFE (React.js) - Extra
- `xqueue` - http://localhost:18040/api/v1/ - Python/Django - Extra
- `analyticspipeline` - http://localhost:4040/ - Python - Extra
- `marketing` - http://localhost:8080/ - PHP/Drupal - Extra

edx docs: https://github.com/edx

Other Commands
-----------------------

1. After the services have started, if you need shell access to one of the services, run ``make <service>-shell``. For example to access the Catalog/Course Discovery Service, you can run:


    make discovery-shell

2. To see logs from containers running in detached mode, you can running the following:


    make logs

3. To view the logs of a specific service container run ``make <service>-logs``. For example, to access the logs for Ecommerce, you can run:


    make ecommerce-logs

4. To reset your environment and start provisioning from scratch, you can run:


    make destroy

5. For information on all the available ``make`` commands, you can run:


    make help

6. To stop all services, run following command:


    make down

Useful Commands
---------------

``make dev.up`` can take a long time, as it starts all services
``make dev.up.<service>`` it starts a single service and its dependencies
``make dev.pull`` can take a long time, as it pulls all services' images
``make dev.pull.<service>`` pull images required by your service and its dependencies
``make dev.provision.services.<service1>+<service2>+...``
``make dev.provision`` in order to run an expedited version of provisioning for a specific set of services.
``docker-compose restart`` Sometimes you may need to restart a particular application server

    docker-compose restart <service>

In all the above commands, ``<service>`` should be replaced with one of the following:

-  credentials
-  discovery
-  ecommerce
-  lms
-  edx_notes_api
-  studio
-  registrar
-  gradebook
-  program-console
-  frontend-app-learning
-  frontend-app-publisher

If you'd like to add some convenience make targets, you can add them to a ``local.mk`` file, ignored by git.
